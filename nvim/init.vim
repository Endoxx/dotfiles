" General
source $HOME/.config/nvim/settings/general.vim
source $HOME/.config/nvim/settings/mapping.vim
source $HOME/.config/nvim/vim-plug/plugins.vim

" Theme
source $HOME/.config/nvim/themes/onedark.vim

" Pugin configurations
source $HOME/.config/nvim/settings/plugins/airline.vim

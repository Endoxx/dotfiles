#!/bin/zsh
#===============================================================================
#
#             NOTES: For this to work you must have cloned the gitLab
#                    repo to your home folder as ~/.dotfiles/
#
#===============================================================================

#==============
# Variables
#==============

dotfiles_dir=~/.dotfiles
log_file=~/install_progress_log.txt


#==============
# Delete existing dot files and folders
#==============

sudo rm -rf ~/.config/nvim > /dev/null 2>&1
sudo rm -rf ~/.zshrc
sudo rm -rf ~/.zsh_prompt


#==============
# Create symlinks in the home folder
# Allow overriding with files of matching names in the custom-configs dir
#==============

ln -sf $dotfiles_dir/nvim ~/.config/nvim
ln -sf $dotfiles_dir/zsh/zshrc ~/.zshrc
ln -sf $dotfiles_dir/zsh/zsh_prompt ~/.zsh_prompt


#==============
# Set zsh as the default shell
#==============

# sudo chsh -s /bin/zsh


#==============
# Give the user a summary of what has been installed
#==============
echo -e "\n======== Summary ========\n"
#cat $log_file
echo
echo "Finished!!!"
#rm $log_file


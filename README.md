# Dotfiles

This is my attempt to unify all the dotfiles across all of my machines.
I'm trying to make them portable and easy to install.

## Installation

``` bash
git clone https://gitlab.com/Endoxx/dotfiles.git ~/.dotfiles
cd ~/.dotfiles/install-scripts
bash create-symlinks.sh
```

# Contents

For the time being there are only NeoVim and Zsh included,
but I'll probably expand my dotfiles in the near future.

## Zsh

- Work in progress.

## NeoVim

- Work in progress.
